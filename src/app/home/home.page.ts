import { Component } from '@angular/core';
// import { NavController } from 'ionic-angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  qrdata: any;
  createCode: any;

  constructor() {}

   public create() {
    this.createCode =this.qrdata;
    }
    
    public clear() {
    this.createCode ='';
    }

}
